TestIterator Questions:
 1) Try with a LinkedList - does it make any difference behaviorally? (ignore performance)
    * Behaviorally nothing changes, all tests still pass

 2) What happens if you use list.remove(Integer.valueOf(77))?
    * If I put it in the loop the test fails, throwing an exception (ConcurrentModificationException)
    * If I get rid of the loop and put it in by itself, I observe that it remove the first found 77 value in the list

TestList Questions:
 1) Try with a LinkedList - does it make any difference behaviorally? (ignore performance)
    * Behaviorally nothing changes, all tests still pass

 2) list.remove(5); what does this method do?
    * Removes the value from the list at index 5

 3) list.remove(Integer.valueOf(5)); what does this one do?
    * Removes the first occurrence of value = 5 from the list

TestPerformance Questions:
 1) Record running times:
    * Runtimes for each SIZE are commented below every test in TestPerformance.java

 2) Which of the two lists performs better for each method as the size increases?
    * Add/Remove: LinkedList
    * Access: ArrayList