package cs271.lab.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPerformance {

  //Total running times for SIZE = 10, 100, 1000, 10000:
  //10, 1_000_000 = 129 ms
  //100, 1_000_000 = 146 ms
  //1000, 1_000_000 = 568 ms
  //10000, 1_000_000 = 6 s 258 ms

  private final int SIZE = 10000;

  private final int REPS = 1_000_000;

  private List<Integer> arrayList;

  private List<Integer> linkedList;

  @Before
  public void setUp() {
    arrayList = new ArrayList<Integer>(SIZE);
    linkedList = new LinkedList<Integer>();
    for (int i = 0; i < SIZE; i++) {
      arrayList.add(i);
      linkedList.add(i);
    }
  }

  @After
  public void tearDown() {
    arrayList = null;
    linkedList = null;
  }

  @Test
  public void testLinkedListAddRemove() {
    for (int r = 0; r < REPS; r++) {
      linkedList.add(0, 77);
      linkedList.remove(0);
    }
  }
  //10: 65 ms
  //100: 60 ms
  //1000: 49 ms
  //10000: 70 ms

  @Test
  public void testArrayListAddRemove() {
    for (int r = 0; r < REPS; r++) {
      arrayList.add(0, 77);
      arrayList.remove(0);
    }
  }
  //10: 59 ms
  //100: 65 ms
  //1000: 146 ms
  //10000: 1 s 211 ms

  @Test
  public void testLinkedListAccess() {
    long sum = 0;
    for (int r = 0; r < REPS; r++) {
      sum += linkedList.get(r % SIZE);
    }
  }
  //10: 27 ms
  //100: 47 ms
  //1000: 363 ms
  //10000: 4 s 889 ms

  @Test
  public void testArrayListAccess() {
    long sum = 0;
    for (int r = 0; r < REPS; r++) {
      sum += arrayList.get(r % SIZE);
    }
  }
  //10: 24 ms
  //100: 27 ms
  //1000: 22 ms
  //10000: 21 ms
}
